/********************
* GLOBAL VARIABLES
********************/
var Syos = "/RSC/SYOS/syos.aspx";
var perfNo = window.location.search;
var CDN = "//cdn2.rsc.org.uk/sitefinity/images/rsc/syos";
var persistentLink = "";
var isMobile;
var isSubmitting = false;
var isLoading = false;
var isRemoving = false;
var isSyosLoading = false;

// REFACTOR TO BUILD LINKS FROM JAVASCRIPT OBJECT???

jQuery(document).ready(function () {

    if (persistentLink == "") {
        ShowTimer();

        if (isTouchDevice() === true) {
            isMobile = true;
            var mobileQS = "&mobile=true";
        }

        if (isMobile) {
            perfNo = perfNo + mobileQS;
        }

        if (!Modernizr.inlinesvg) {
            perfNo = perfNo + "&syos=false";
        }

        try {
            var theLink = Syos + perfNo;
            persistentLink = theLink;

            isSyosLoading = true;
            jQuery("#syos-wrapper").load(theLink);
        } catch (e) {
            console.log(e);
        }
    }

    /********************    
    * AJAX EVENT HANDLERS    
    ********************/

    jQuery(document).ajaxSend(function () {
        /************************************************        
        * Fires before an AJAX request starts        
        ************************************************/

        // AJAX send handler maybe useful in the future
    });

    jQuery(document).ajaxStart(function () {
        /************************************************        
        * Fires when an AJAX request is started        
        ************************************************/

        if (isSyosLoading) {
            ShowTimer();
        }
    });

    jQuery(document).ajaxSuccess(function () {
        /************************************************        
        * Only fires when an AJAX request is successful        
        ************************************************/

        if (!Modernizr.inlinesvg) {
            jQuery(".no-syos").show();
        }

        // Focus event called on click, messing up SYOS on iOS devices
        // possibly others too. So this should stop those problems
        jQuery(".seat").unbind("focus").on("focus", function (e) {
            return false;
        });


        /********************         
         * SYOS         
         ********************/

        // Choosing a Visual Level
        jQuery("#AuditoriaLevels .levels-svg .seat-level").unbind("click").click(function (e) {
            e.preventDefault;

            if (!isLoading) {
                var $this = jQuery(this);
                var isAvailable = $this.data("available");

                if (isAvailable == true) {
                    try {
                        var $this = jQuery(this);
                        var Level = $this.data("level");
                        var theLink = Syos + perfNo + "&level=" + Level;

                        if (isMobile) {
                            theLink = theLink + mobileQS;
                        }

                        persistentLink = theLink;

                        try {
                            isSyosLoading = true;
                            jQuery("#syos-wrapper").load(theLink, function () {
                                jQuery(document).ajaxSuccess(function () {
                                    initToolTip();
                                });

                                jQuery(document).ajaxStop(function () {
                                    // fire gimmeGo when ajax has completed
                                    if (isMobile) {
                                        gimmeGo($("#blocks"));
                                    } else {
                                        console.log("here");
                                        gimmeGo($("#SyosPanel"));
                                    }

                                    // use pure javascript to manipulate svg DOM as jQuery won't until V3.
                                    // Have to add or remove classes once the DOM is loaded, i.e after AJAX load

                                    // Remove selected class if one has already been selected
                                    var item = jQuery(".seat-level.selected").attr("id");
                                    if (item != undefined) {
                                        document.getElementById(item).setAttribute("class", "seat-level");
                                    }

                                    // add selected class to the clicked block
                                    try {
                                        document.getElementById("level-" + Level).setAttribute("class", "seat-level selected");
                                    } catch (e) {
                                    }
                                });
                            });
                        } catch (e) {
                            console.log(e);
                        }
                    } catch (e) {
                        console.log(e);
                    }
                } else {
                    alert("Currently not available");
                }
            }

            return false;
        });

        // Choosing a SYOS level
        jQuery("#AuditoriaLevels a").unbind("click").click(function (e) {
            e.preventDefault;

            if (!isLoading) {
                var $this = jQuery(this);
                var hasAvailability = !$this.hasClass("unavailable");

                if (hasAvailability) {
                    try {
                        var $this = jQuery(this);
                        var theLink = $this.data("href");

                        if (isMobile) {
                            theLink = theLink + mobileQS;
                        }

                        persistentLink = theLink;

                        try {
                            isSyosLoading = true;
                            jQuery("#syos-wrapper").load(theLink, function () {
                                jQuery(document).ajaxStop(function () {
                                    // fire gimmeGo when ajax load has totally completed
                                    if (isMobile) {
                                        gimmeGo($("#section-blocks"));
                                    } else {
                                        gimmeGo($("#SyosPanel"));
                                    }
                                });
                            });
                        } catch (e) {
                            console.log(e);
                        }
                    } catch (e) {
                        console.log(e);
                    }
                } else {
                    alert("Currently not available");
                }
            }

            return false;
        });

        // Choosing a mobile SYOS block
        jQuery("#section-blocks .block").unbind("click").click(function (e) {
            e.preventDefault;

            if (!isLoading) {
                try {
                    var $this = $(this);

                    var hasAvailability = document.getElementById(this.id).getAttribute("data-hasAvailability");

                    if (hasAvailability != "false") {
                        var Level = $this.data("level");
                        var Block = $this.data("block");

                        var theLink = Syos + perfNo + "&level=" + Level + "&block=" + Block;

                        persistentLink = theLink;

                        try {
                            isSyosLoading = true;
                            jQuery("#syos-wrapper").load(theLink, function (response, status, xhr) {
                                jQuery(document).ajaxStop(function () {
                                    // fire gimmeGo when ajax has completed
                                    if (isMobile) {
                                        gimmeGo($("#SyosPanel"));
                                    }


                                    // use pure javascript to manipulate svg DOM as jQuery won't until V3.
                                    // Have to add or remove classes once the DOM is loaded, i.e after AJAX load

                                    // Remove selected class if one has already been selected
                                    var item = jQuery(".block.selected-block").attr("id");
                                    if (item != undefined) {
                                        document.getElementById(item).setAttribute("class", "block");
                                    }

                                    // add selected class to the clicked block
                                    try {
                                        document.getElementById("level-" + Level + "-block-" + Block).setAttribute("class", "block selected-block");
                                    } catch (e) {
                                    }

                                    var Facility = jQuery("#facilityNumber").val();
                                    jQuery("#svg").attr("name", "block_" + Facility + "_" + Level + "_" + Block);
                                });
                            });
                        } catch (e) {
                            console.log(e);
                        }
                    } else {
                        alert("Sorry, this block has no available seats.");
                    }

                } catch (e) {
                    console.log(e);
                }
            }

            return false;
        });

        // Seat MouseOver
        jQuery(".seat").unbind("mouseover").mouseover(function (e) {
            var $this = jQuery(this);

            if (!isMobile) {
                ShowTip($this.attr("id"));
            }
        });

        // Seat MouseOut
        jQuery(".seat").unbind("mouseout").mouseout(function (e) {
            var $this = jQuery(this);

            if (!isMobile) {
                HideTip($this.attr("id"));
            }
        });

        // Clicking  a seat
        jQuery(".seat").unbind("click").click(function (e) {
            e.preventDefault;

            if (!isLoading) {
                try {
                    var $this = jQuery(this);
                    var SeatIsAvailable = $this.data("availability") == "avl";

                    if (SeatIsAvailable) {
                        jQuery(this).toggleClass("selected");
                        //jQuery(this).next().toggle();

                        var SeatId = $this.attr("id");
                        var Zone = $this.data("zone");
                        var Legend = $this.data("legend");
                        var Level = $this.data("level");
                        var SeatName = $this.attr("title");

                        // centralize the popup over the seat map
                        var x = Math.floor(jQuery("#MapContainer").outerWidth() / 2);
                        var y = Math.floor(jQuery("#MapContainer").outerHeight() / 2) - 150;

                        if (screenWidth < 480) {
                            x = 0;
                        } else {
                            x -= 200;
                        }

                        try {
                            var theLink = Syos + perfNo + "&level=" + Level + "&seat=" + SeatId + "&zone=" + Zone;

                            if (isMobile) {
                                var SeatingBlock = $this.data("block");

                                theLink = theLink + "&block=" + SeatingBlock;
                            }

                            isSyosLoading = true;
                            jQuery("#syos-wrapper").load(theLink, function () {
                                ShowToolTip(Legend, Level, SeatName, x, y);

                                jQuery(document).ajaxStop(function () {
                                    // fire gimmeGo when ajax has completed                                    
                                    gimmeGo($("#seat-information"));
                                });
                            });
                        } catch (e) {
                            console.log(e);
                        }
                    }
                } catch (e) {
                    console.log(e);
                }
            }

            return false;
        });

        // Selecting a pricetype from seat info dialog
        jQuery("#pricetype-table .pricetype-line a").unbind("click").click(function (e) {
            e.preventDefault;

            if (!isLoading) {
                try {
                    var $this = jQuery(this);
                    var theLink = $this.data("href");

                    if (isMobile) {
                        theLink = theLink + mobileQS;
                    }

                    try {
                        isSyosLoading = true;
                        jQuery("#syos-wrapper").load(theLink, function () {
                            jQuery(document).ajaxStop(function () {
                                // fire gimmeGo when ajax has completed
                                gimmeGo($("#SeatSummary"));
                            });
                        });
                    } catch (e) {
                        console.log(e);
                    }
                } catch (e) {
                    console.log(e);
                }

            }
            return false;
        });

        // Close Seat Info
        jQuery("#seat-information #info #close").unbind("click").click(function (e) {
            try {
                RemoveSelectedClasses(jQuery("#close").data("seat"));
            } catch (e) {
                console.log(e);
            }

            jQuery("#seat-information").hide();
        });

        // Remove a selected seat
        jQuery("#seat-selection-summary a").unbind("click").click(function (e) {
            e.preventDefault;

            if (!isLoading) {
                try {
                    var $this = jQuery(this);
                    var theLink = $this.data("href");

                    if (isMobile) {
                        theLink = theLink + mobileQS;
                    }

                    isRemoving = true;

                    try {
                        isSyosLoading = true;
                        jQuery("#syos-wrapper").load(theLink, function () {
                            jQuery(document).ajaxStop(function () {
                                // fire gimmeGo when ajax has completed                                
                                if (jQuery(".summary-row").length > 0) {
                                    gimmeGo($("#Summary"));
                                } else {
                                    gimmeGo($(".PriceTypeKey"));
                                }
                            });

                            var array = theLink.split("&");

                            // Split the seat/remove qs argument into array
                            // slight concern that referencing by index could fail
                            // maybe rethink...
                            var idArray = array[2].split("=");

                            // take the seat id number from the array
                            var theID = idArray[1].toString();

                            RemoveSelectedClasses(theID);

                        });
                    } catch (e) {
                        console.log(e);
                    }
                }
                catch (e) {
                    console.log(e);
                }
            }

            return false;
        });

        // Prompt to choose more seats
        jQuery("#select-more").unbind("click").click(function (e) {
            e.preventDefault;

            gimmeGo($("#SyosPanel"));

            return false;
        });


        /********************        
        * BEST AVAILABLE        
        ********************/

        // Selecting a zone
        jQuery("#BestAvailablePanel input[type='radio']").unbind("click").click(function (e) {
            try {
                $this = jQuery(this);
                var theLink = $this.val();

                if (isMobile) {
                    theLink = theLink + mobileQS;
                }

                jQuery("#SelectedZone").val(theLink);
            } catch (e) {
                console.log(e);
            }
        });

        // Selecting a seat quantity
        jQuery("#BestAvailablePanel select").unbind("click").change(function (e) {
            try {
                var $this = jQuery(this);

                // Create temporary arrays of price types & quantities
                // for indexing & assigning
                var $pricetypeArray = jQuery("#SelectedPriceType").val().split(",");
                var $quantitiesArray = jQuery("#SelectedQuantity").val().split(",");

                // Get the selected quantity
                var $quantity = $this.find(":selected").text();

                // Get the price type id
                var $pricetypeId = $this.data("pricetype").toString();

                // find the array index of the selected price type
                var $quantityTarget = $pricetypeArray.indexOf($pricetypeId);

                // Assign the new quantity value to the quantity array
                $quantitiesArray[$quantityTarget] = $quantity;

                // Write the new quantity values back to the hidden field
                jQuery("#SelectedQuantity").val($quantitiesArray.toString());
            } catch (e) {
                console.log(e);
            }
        });



        /********************************        
        * SYOS & BEST AVAILABLE SWITCHES        
        ********************************/

        jQuery("#BestAvailableNotePanel a").unbind("click").click(function (e) {
            e.preventDefault;

            if (!isLoading) {
                try {
                    var $this = jQuery(this);
                    var theLink = jQuery(this).data("href");

                    if (isMobile) {
                        theLink = theLink + mobileQS;
                    }

                    persistentLink = theLink;

                    try {
                        isSyosLoading = true;
                        jQuery("#syos-wrapper").load(theLink);
                    } catch (e) {
                        console.log(e);
                    }
                } catch (e) {
                    console.log(e);
                }
            }

            return false;
        });

        jQuery("#SyosNotePanel a").unbind("click").click(function (e) {
            e.preventDefault;

            if (!isLoading) {
                try {
                    var $this = jQuery(this);
                    var theLink = jQuery(this).data("href");

                    if (isMobile) {
                        theLink = theLink + mobileQS;
                    }

                    persistentLink = theLink;

                    try {
                        isSyosLoading = true;
                        jQuery("#syos-wrapper").load(theLink);
                    } catch (e) {
                        console.log(e);
                    }
                } catch (e) {
                    console.log(e);
                }
            }

            return false;
        });
    });

    jQuery(document).ajaxComplete(function () {
        /************************************************        
        * Fired any time an AJAX request completes. Successfully or not.        
        ************************************************/

        // AJAX complete handler might be useful in the future
    });

    jQuery(document).ajaxStop(function () {
        /************************************************        
        * Fires when *ALL* AJAX request are completed        
        ************************************************/

        if (isSubmitting == false) {
            HideTimer();
        }

        if (isRemoving == true) {
            HideTimer();
        }

        if (isSyosLoading) {
            HideTimer();
        }

        // SYOS add to basket
        jQuery(".SyosSubmit").unbind("click").click(function (e) {
            e.preventDefault;

            if (!isLoading) {
                try {
                    ShowTimer();
                    isSubmitting = true;

                    var $this = jQuery(this);
                    var theLink = $this.attr("href");

                    if (isMobile) {
                        theLink = theLink + mobileQS;
                    }

                    try {
						isSyosLoading = true;
                        jQuery("#syos-wrapper").load(theLink);
                    } catch (e) {
                        console.log(e);
                    }
                } catch (e) {
                    console.log(e);
                }
            }

            return false;
        });


        // BA add to basket
        jQuery("#BestAvailablePanel button").unbind("click").click(function (e) {
            e.preventDefault;

            if (!isLoading) {
                try {
                    ShowTimer();
                    isSubmitting = true;

                    var $this = jQuery(this);

                    var bestAvailable = "&bestavailable=true";
                    var priceType = "&selectedpricetypes=" + jQuery("#SelectedPriceType").val();
                    var zone = "&zone=" + jQuery("#SelectedZone").val();
                    var quantity = "&quantities=" + jQuery("#SelectedQuantity").val();
                    var theLink = Syos + perfNo + bestAvailable + priceType + zone + quantity;

                    try {
                        isSyosLoading = true;
                        jQuery("#syos-wrapper").load(theLink);
                    } catch (e) {
                        console.log(e);
                    }
                } catch (e) {
                    console.log(e);
                }
            }

            return false;
        });
    });

    jQuery(document).ajaxError(function () {
        /************************************************        
        * Fires when an AJAX request errors        
        ************************************************/

        // Show a friendly error message!
    });
});


/********************
* FUNCTIONS
********************/

function isTouchDevice() {
    return true == (("ontouchstart" in window
        || window.DocumentTouch && document instanceof DocumentTouch)
        || (navigator.maxTouchPoints > 0)
        || (navigator.msMaxTouchPoints > 0));
}

function SetRelevantTitle(isTimed) {
    jQuery(".base-header h1").html("BUY TICKETS");

    console.log("isTimed = " + isTimed);

    if (isTimed == true) {
        jQuery("#infotag").html("TIME SLOTS");
    }
}

function LoadVisualAuditoria(facility, availability, count) {
    try {
        jQuery("#levels-in-facility").load("/RSC/SYOS/facility/" + facility + "/images/svg/" + facility + "_Visual_Auditoria.svg", function () {
            var $availabilityArray = availability.split(",");
            var $countArray = count.split(",");

            for (var i = 0; i < $availabilityArray.length; i++) {
                if ($availabilityArray[i] == "0") {
                    document.getElementById("level-" + i).setAttribute("class", "seat-level unavailable");
                    document.getElementById("level-" + i).setAttribute("data-available", "false");
                } else {
                    document.getElementById("level-" + i).setAttribute("data-available", "true");
                }

                document.getElementById("level-" + i).setAttribute("data-count", $countArray[i].toString());
            }
        });
    } catch (e) {
        console.log(e);
    }
}

function SetBlockAvailability(level, unAvailableBlocks) {
    try {
        try {
            var unAvailableArray = unAvailableBlocks.split(",");

            console.log(level);
            console.log(unAvailableArray);

            for (var i = 0; i <= unAvailableArray.length; i++) {
                if (unAvailableArray[i] > 0) {
                    AddUnavailableClass(level, unAvailableArray[i]);
                }
            }
        } catch (e) {
            console.log(e);
        }
    } catch (e) {
        console.log(e);
    }
}

function AddUnavailableClass(level, id) {
    jQuery(document).ajaxComplete(function () {
        // use pure javascript to manipulate svg DOM as jQuery won't until V3.
        // Have to add or remove classes once the DOM is loaded, i.e after AJAX load

        // add selected class to the block
        try {
            document.getElementById("level-" + level + "-block-" + id.toString()).setAttribute("class", "block unavailable");
            document.getElementById("level-" + level + "-block-" + id.toString()).setAttribute("data-hasAvailability", "false");
        } catch (e) {
            console.log(e);
        }
    });
}

function Load(target, theLink, caller) {
    try {
        jQuery(target).load(theLink);
    } catch (e) {
        console.log(e);
    }
}

function ShowToolTip(Legend, Level, SeatName, X, Y) {
    try {
        var facility = jQuery("#facilityNumber").val();

        jQuery("#seat-information").addClass(Legend);
        jQuery("#seat-information .arrow").css("border-bottom", "20px solid " + jQuery("#seat-information").css("border-left-color"));
        jQuery("#seat-information").css({ "top": Y, "left": X });

        jQuery("#seat-information #vfs img").attr("src", CDN + "/facility/" + facility + "/" + Level + "/" + SeatName.toLowerCase() + ".jpg");
        jQuery("#seat-information #vfs img").attr("alt", "A representitive view from seat " + SeatName);

        jQuery("#seat-information").fadeIn();
    } catch (e) {
        console.log(e);
    }
}

function RemoveSelectedClasses(theID) {
    // We need to preserve the seat classes that are required for availability and price zone
    // so...

    // Get the elements current set of class parameters
    var $seatClasses = document.getElementById(theID).getAttribute("class");

    // replace the elements classes with the original classes without the 'selected' class    
    document.getElementById(theID).setAttribute("class", $seatClasses.replace("selected", ""));


    // Now try and do the same for the sibling pulsing seat, 
    // catching if the pulse is turned off...
    try {
        var $pulseClasses = document.getElementById(theID + "-pulse").getAttribute("class");
        document.getElementById(theID + "-pulse").setAttribute("class", $pulseClasses.replace("selected", ""));
    } catch (e) {
        console.log(e);
        console.log("if this has errored, then seat pulse is most likely switched off.");
    }
}

function ShowTimer() {
    isLoading = true;
    jQuery("#syos-wrapper").addClass("faded");
    jQuery("#syos-loader").show();
    jQuery(".hidden").show();
}

function HideTimer() {
    isLoading = false;
    jQuery("#syos-wrapper").removeClass("faded");
    jQuery(".hidden").hide();
    jQuery("#syos-loader").hide();

    if (isRemoving) {
        isRemoving = false;
    }

    if (isSubmitting) {
        isSubmitting = false;
    }

    isSyosLoading = false;
}

function GoToBasket() {
    ShowTimer();

    try {
        window.location = "/basket/";
    } catch (e) {
        console.log(e);
    }
}

// Hover Tooltip
function initToolTip() {
    try {
        tooltip = document.getElementById('tooltip');
        tooltip_bg = document.getElementById('tooltip_bg');
        tooltip_bg_p = document.getElementById('tooltip_bg_point');
        seatNumb = document.getElementById('seatNumb');
        allSeats = document.getElementsByTagName('circle');
        priceIndic = document.getElementById('priceIndic');
        availIndic = document.getElementById('availIndic');
        theSvg = document.getElementById('svg');
        viewBox = theSvg.getAttribute("viewBox");
        viewBoxArr = viewBox.split(/[\s,]+/);
        svgRightEdge = parseInt(viewBoxArr[2]) - 140;
        svgBottomEdge = parseInt(viewBoxArr[3]) - 96;
    } catch (e) {
        console.log(e);
    }
} // yes TFS, this bracket needs to be here

function ShowTip(id) {
    var seat = document.getElementById(id);

    theX = parseInt(seat.getAttribute("cx"));
    theY = parseInt(seat.getAttribute("cy"));

    if (theX > svgRightEdge) {
        x_tt = theX - 161;
        x_tt_bg = theX - 20 - 155;
        x_tt_pl1 = theX - 7;
        x_tt_pl2 = theX - 21;
        x_tt_pl3 = theX - 21;
    } else {
        x_tt = theX + 36;
        x_tt_bg = theX + 20;
        x_tt_pl1 = theX + 7;
        x_tt_pl2 = theX + 21;
        x_tt_pl3 = theX + 21;
    }

    if (theY > svgBottomEdge) {
        y_tt = theY - 78 + 24;
        y_tt_bg = theY - 78;
        y_tt_pl1 = theY;
        y_tt_pl2 = theY;
        y_tt_pl3 = theY - 16;
    } else {
        y_tt = theY + 24;
        y_tt_bg = theY;
        y_tt_pl1 = theY;
        y_tt_pl2 = theY;
        y_tt_pl3 = theY + 16;
    }

    theSeat = seat.getAttribute("title");

    theAvail = seat.getAttribute("data-availability");
    if (theAvail == "avl") {
        theAvail = "Available";
    } else {
        theAvail = "Not Available";
    }

    priceType = seat.getAttribute("data-legend");

    switch (priceType) {
        case "P":
            priceType = "Premium Seat";
            break;
        case "A":
            priceType = "Price Band A";
            break;
        case "B":
            priceType = "Price Band B";
            break;
        case "C":
            priceType = "Price Band C";
            break;
        case "D":
            priceType = "Price Band D";
            break;
        case "E":
            priceType = "Price Band E";
            break;
        case "S":
            priceType = "Standing";
            break;
        case "W":
            priceType = "Wheelchair Position";
            break;
        case "N":
            priceType = "Wheelchair Companion";
            break;
        default:
            priceType = "";
            break;
    }

    tooltip.setAttributeNS(null, "x", x_tt);
    tooltip.setAttributeNS(null, "y", y_tt);
    priceIndic.setAttributeNS(null, "x", x_tt);
    availIndic.setAttributeNS(null, "x", x_tt);
    seatNumb.firstChild.data = theSeat;
    availIndic.firstChild.data = theAvail;
    priceIndic.firstChild.data = priceType;
    tooltip.setAttributeNS(null, "class", "tooltip vis");
    tooltip.setAttributeNS(null, "visibility", "visible");

    tooltip_bg.setAttributeNS(null, "x", x_tt_bg);
    tooltip_bg.setAttributeNS(null, "y", y_tt_bg);
    tooltip_bg.setAttributeNS(null, "class", "tooltip_bg vis");
    tooltip_bg.setAttributeNS(null, "visibility", "visible");

    pointList = [];
    pointList.push([x_tt_pl1, y_tt_pl1]);
    pointList.push([x_tt_pl2, y_tt_pl2]);
    pointList.push([x_tt_pl3, y_tt_pl3]);

    polyPoints = [];

    for (var i = 0, l = pointList.length; i < l; i++) {
        polyPoints.push(pointList[i].join(','));
    }
    polyPoints.join(' ');

    tooltip_bg_p.setAttributeNS(null, "points", polyPoints);
    tooltip_bg_p.setAttributeNS(null, "class", "tooltip_bg vis");
    tooltip_bg_p.setAttributeNS(null, "visibility", "visible");
}

function HideTip(id) {
    tooltip.setAttributeNS(null, "class", "tooltip");
    tooltip_bg.setAttributeNS(null, "class", "tooltip_bg");
    tooltip_bg_p.setAttributeNS(null, "class", "tooltip_bg");
    tooltip.setAttributeNS(null, "visibility", "hidden");
    tooltip_bg.setAttributeNS(null, "visibility", "hidden");
    tooltip_bg_p.setAttributeNS(null, "visibility", "hidden");
}
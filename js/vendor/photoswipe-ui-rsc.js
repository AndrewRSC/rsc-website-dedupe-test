/*! 
* PhotoSwipe RSC UI
* 
* built on:
* 
* PhotoSwipe Default UI - 4.1.1 - 2015-12-24
* http://photoswipe.com
* Copyright (c) 2015 Dmitry Semenov; */
/**
*
* UI on top of main sliding area (caption, arrows, close button, etc.).
* Built just using public methods/properties of PhotoSwipe.
* 
*/
(function (root, factory) { 
	if (typeof define === 'function' && define.amd) {
		define(factory);
	} else if (typeof exports === 'object') {
		module.exports = factory();
	} else {
		root.PhotoSwipeUI_RSC = factory();
	}
})(this, function () {

	'use strict';



var PhotoSwipeUI_RSC =
 function(pswp, framework) {

	var ui = this;
	var _overlayUIUpdated = false,
		_controlsVisible = true,
		_fullscrenAPI,
		_controls,
		_galleryTitle,
		_captionContainer,
		_fakeCaptionContainer,
        _captionTog,
        _captionHidden = false,
		_indexIndicator,
		_shareButton,
		_shareModal,
		_shareModalHidden = true,
		_initalCloseOnScrollValue,
		_isIdle,
		_listen,
        
        _thumbsContainer,
        _thumbsInner,
        _thumbTog,
        _thumbHidden = false,
        _thumbArray = [],

		_loadingIndicator,
		_loadingIndicatorHidden,
		_loadingIndicatorTimeout,

		_galleryHasOneSlide,

		_options,
		_defaultUIOptions = {
			barsSize: {top:44, bottom:'auto'},
			closeElClasses: ['item', 'caption', 'zoom-wrap', 'ui', 'top-bar'], 
			timeToIdle: 4000, 
			timeToIdleOutside: 1000,
			loadingIndicatorDelay: 1000, // 2s
			
			addCaptionHTMLFn: function(item, captionEl /*, isFake */) {
				if(!item.title) {
					captionEl.children[1].innerHTML = '';
					return false;
				}
				captionEl.children[1].innerHTML = item.title;
                //var capCenter = captionEl.children[1],
                //    capInner = capCenter.children[1],
                //    capContent = capInner.children[0];
                //
                //capContent.innerHTML = item.title;
                //
                //$("#captionCenter").simplebar('recalculate');
				
                return true;
			},

			closeEl:true,
			captionEl: true,
			zoomEl: true,
			shareEl: false,
			counterEl: true,
			arrowEl: true,
			preloaderEl: true,

			tapToClose: false,
			tapToToggleControls: true,

			clickToCloseNonZoomable: true,

			shareButtons: [
				{id:'facebook', label:'Share on Facebook', url:'https://www.facebook.com/sharer/sharer.php?u={{url}}'},
				{id:'twitter', label:'Tweet', url:'https://twitter.com/intent/tweet?text={{text}}&url={{url}}'},
				{id:'pinterest', label:'Pin it', url:'http://www.pinterest.com/pin/create/button/'+
													'?url={{url}}&media={{image_url}}&description={{text}}'},
				{id:'download', label:'Download image', url:'{{raw_image_url}}', download:true}
			],
			getImageURLForShare: function( /* shareButtonData */ ) {
				return pswp.currItem.src || '';
			},
			getPageURLForShare: function( /* shareButtonData */ ) {
				return window.location.href;
			},
			getTextForShare: function( /* shareButtonData */ ) {
				return pswp.currItem.title || '';
			},
				
			indexIndicatorSep: '/',
			fitControlsWidth: 1200

		},
		_blockControlsTap,
		_blockControlsTapTimeout;



        var _onControlsTap = function(e) {
            if(_blockControlsTap) {
                return true;
            }
           
            e = e || window.event;
           
            if(_options.timeToIdle && _options.mouseUsed && !_isIdle) {
                // reset idle timer
                _onIdleMouseMove();
            }
           
            var target = e.target || e.srcElement,
                uiElement,
                clickedClass = target.getAttribute('class') || '',
                found;
            
            if (clickedClass == '') {
                var parClass = target.parentElement.getAttribute('class');
                if (parClass.indexOf('pswp__') > -1 ) {
                    clickedClass = parClass;
                }
            }
            
            for(var i = 0; i < _uiElements.length; i++) {
                uiElement = _uiElements[i];
                
                if(uiElement.onTap && clickedClass.indexOf('pswp__' + uiElement.name ) > -1 ) {
                    uiElement.onTap();
                    found = true;
                }
            }
           
            if(found) {
                if(e.stopPropagation) {
                    e.stopPropagation();
                }
                
                _blockControlsTap = true;
                
                // Some versions of Android don't prevent ghost click event 
                // when preventDefault() was called on touchstart and/or touchend.
                // 
                // This happens on v4.3, 4.2, 4.1, 
                // older versions strangely work correctly, 
                // but just in case we add delay on all of them)	
                var tapDelay = framework.features.isOldAndroid ? 600 : 30;
                _blockControlsTapTimeout = setTimeout(function() {
                    _blockControlsTap = false;
                }, tapDelay);
            }
        },
		_fitControlsInViewport = function() {
			return !pswp.likelyTouchDevice || _options.mouseUsed || screen.width > _options.fitControlsWidth;
		},
		_togglePswpClass = function(el, cName, add) {
			framework[ (add ? 'add' : 'remove') + 'Class' ](el, 'pswp__' + cName);
		},

		// add class when there is just one item in the gallery
		// (by default it hides left/right arrows and 1ofX counter)
		_countNumItems = function() {
			var hasOneSlide = (_options.getNumItemsFn() === 1);

			if(hasOneSlide !== _galleryHasOneSlide) {
				_togglePswpClass(_controls, 'ui--one-slide', hasOneSlide);
				_galleryHasOneSlide = hasOneSlide;
			}
		},
		_toggleShareModalClass = function() {
			_togglePswpClass(_shareModal, 'share-modal--hidden', _shareModalHidden);
		},
		_toggleShareModal = function() {

			_shareModalHidden = !_shareModalHidden;
			
			
			if(!_shareModalHidden) {
				_toggleShareModalClass();
				setTimeout(function() {
					if(!_shareModalHidden) {
						framework.addClass(_shareModal, 'pswp__share-modal--fade-in');
					}
				}, 30);
			} else {
				framework.removeClass(_shareModal, 'pswp__share-modal--fade-in');
				setTimeout(function() {
					if(_shareModalHidden) {
						_toggleShareModalClass();
					}
				}, 300);
			}
			
			if(!_shareModalHidden) {
				_updateShareURLs();
			}
			return false;
		},

		//_openWindowPopup = function(e) {
		//	e = e || window.event;
		//	var target = e.target || e.srcElement;
//
		//	pswp.shout('shareLinkClick', e, target);
//
		//	if(!target.href) {
		//		return false;
		//	}
//
		//	if( target.hasAttribute('download') ) {
		//		return true;
		//	}
//
		//	window.open(target.href, 'pswp_share', 'scrollbars=yes,resizable=yes,toolbar=no,'+
		//								'location=yes,width=550,height=420,top=100,left=' + 
		//								(window.screen ? Math.round(screen.width / 2 - 275) : 100)  );
//
		//	if(!_shareModalHidden) {
		//		_toggleShareModal();
		//	}
		//	
		//	return false;
		//},
		_updateShareURLs = function() {
			var shareButtonOut = '',
				shareButtonData,
				shareURL,
				image_url,
				page_url,
				share_text;

			for(var i = 0; i < _options.shareButtons.length; i++) {
				shareButtonData = _options.shareButtons[i];

				image_url = _options.getImageURLForShare(shareButtonData);
				page_url = _options.getPageURLForShare(shareButtonData);
				share_text = _options.getTextForShare(shareButtonData);

				shareURL = shareButtonData.url.replace('{{url}}', encodeURIComponent(page_url) )
									.replace('{{image_url}}', encodeURIComponent(image_url) )
									.replace('{{raw_image_url}}', image_url )
									.replace('{{text}}', encodeURIComponent(share_text) );

				shareButtonOut += '<a href="' + shareURL + '" target="_blank" '+
									'class="pswp__share--' + shareButtonData.id + '"' +
									(shareButtonData.download ? 'download' : '') + '>' + 
									shareButtonData.label + '</a>';

				if(_options.parseShareButtonOut) {
					shareButtonOut = _options.parseShareButtonOut(shareButtonData, shareButtonOut);
				}
			}
			_shareModal.children[0].innerHTML = shareButtonOut;
			//_shareModal.children[0].onclick = _openWindowPopup;

		},
		_hasCloseClass = function(target) {
			for(var  i = 0; i < _options.closeElClasses.length; i++) {
				if( framework.hasClass(target, 'pswp__' + _options.closeElClasses[i]) ) {
					return true;
				}
			}
		},
		_idleInterval,
		_idleTimer,
		_idleIncrement = 0,
		_onIdleMouseMove = function() {
			clearTimeout(_idleTimer);
			_idleIncrement = 0;
			if(_isIdle) {
				ui.setIdle(false);
			}
		},
		_onMouseLeaveWindow = function(e) {
			e = e ? e : window.event;
			var from = e.relatedTarget || e.toElement;
			if (!from || from.nodeName === 'HTML') {
				clearTimeout(_idleTimer);
				_idleTimer = setTimeout(function() {
					ui.setIdle(true);
				}, _options.timeToIdleOutside);
			}
		},
		_setupLoadingIndicator = function() {
			// Setup loading indicator
			if(_options.preloaderEl) {
			
				_toggleLoadingIndicator(true);

				_listen('beforeChange', function() {

					clearTimeout(_loadingIndicatorTimeout);

					// display loading indicator with delay
					_loadingIndicatorTimeout = setTimeout(function() {

						if(pswp.currItem && pswp.currItem.loading) {

							if( !pswp.allowProgressiveImg() || (pswp.currItem.img && !pswp.currItem.img.naturalWidth)  ) {
								// show preloader if progressive loading is not enabled, 
								// or image width is not defined yet (because of slow connection)
								_toggleLoadingIndicator(false); 
								// items-controller.js function allowProgressiveImg
							}
							
						} else {
							_toggleLoadingIndicator(true); // hide preloader
						}

					}, _options.loadingIndicatorDelay);
					
				});
				_listen('imageLoadComplete', function(index, item) {
					if(pswp.currItem === item) {
						_toggleLoadingIndicator(true);
					}
				});

			}
		},
		_toggleLoadingIndicator = function(hide) {
			if( _loadingIndicatorHidden !== hide ) {
				_togglePswpClass(_loadingIndicator, 'preloader--active', !hide);
				_loadingIndicatorHidden = hide;
			}
		},
		_applyNavBarGaps = function(item) {
			var gap = item.vGap;

			if( _fitControlsInViewport() ) {
				
				var bars = _options.barsSize; 
				if(_options.captionEl && bars.bottom === 'auto') {
					if(!_fakeCaptionContainer) {
						_fakeCaptionContainer = framework.createEl('pswp__caption pswp__caption--fake');
						_fakeCaptionContainer.appendChild( framework.createEl('pswp__caption-header') );
						_fakeCaptionContainer.appendChild( framework.createEl('pswp__caption__center') );
						_controls.insertBefore(_fakeCaptionContainer, _captionContainer);
						framework.addClass(_controls, 'pswp__ui--fit');
                        
                        //$(".pswp__caption--fake .pswp__caption__center").simplebar();
					}
					if( _options.addCaptionHTMLFn(item, _fakeCaptionContainer, true) ) {

						var captionSize = _fakeCaptionContainer.clientHeight;
						gap.bottom = parseInt(captionSize,10) || 44;
					} else {
						gap.bottom = bars.top; // if no caption, set size of bottom gap to size of top
					}
				} else {
					gap.bottom = bars.bottom === 'auto' ? 0 : bars.bottom;
				}
				
				// height of top bar is static, no need to calculate it
				gap.top = bars.top;
			} else {
				gap.top = gap.bottom = 0;
			}
		},
		_setupIdle = function() {
			// Hide controls when mouse is used
			if(_options.timeToIdle) {
				_listen('mouseUsed', function() {
					
					framework.bind(document, 'mousemove', _onIdleMouseMove);
					framework.bind(document, 'mouseout', _onMouseLeaveWindow);

					_idleInterval = setInterval(function() {
						_idleIncrement++;
						if(_idleIncrement === 2) {
							ui.setIdle(true);
						}
					}, _options.timeToIdle / 2);
				});
			}
		},
		_setupHidingControlsDuringGestures = function() {

			// Hide controls on vertical drag
			_listen('onVerticalDrag', function(now) {
				if(_controlsVisible && now < 0.95) {
					ui.hideControls();
				} else if(!_controlsVisible && now >= 0.95) {
					ui.showControls();
				}
			});

			// Hide controls when pinching to close
			var pinchControlsHidden;
			_listen('onPinchClose' , function(now) {
				if(_controlsVisible && now < 0.9) {
					ui.hideControls();
					pinchControlsHidden = true;
				} else if(pinchControlsHidden && !_controlsVisible && now > 0.9) {
					ui.showControls();
				}
			});

			_listen('zoomGestureEnded', function() {
				pinchControlsHidden = false;
				if(pinchControlsHidden && !_controlsVisible) {
					ui.showControls();
				}
			});
		},
        _toggleCaptionContainer = function(){
            
            var capCent = framework.getChildByClass(_captionContainer, 'pswp__caption__center');
            
            if (!_captionHidden) {
                
                TweenLite.to(capCent, 0.2, {
                    opacity: 0
                });
                TweenLite.to(_captionContainer, 0.2, {
                    height: 0,
                    delay: 0.2,
                    onComplete: function() {
                        _togglePswpClass(_captionContainer, 'hide', !_captionHidden);
                        _captionHidden = !_captionHidden;
                        capCent.style.opacity = '';
                    },
                    clearProps:"all"
                });
                
                //console.log(_captionHidden);
                
            } else {
                
                //console.log(_captionHidden);
                
                _togglePswpClass(_captionContainer, 'hide', !_captionHidden);
                _captionHidden = !_captionHidden;
                
                TweenLite.from(_captionContainer, 0.2, {
                    height: 0,
                    clearProps: "all"
                });
                TweenLite.from(capCent, 0.2, {
                    opacity: 0,
                    delay: 0.2,
                    clearProps: "all"
                });
            }
            
        },
        _toggleThumbs = function(){
            
            _togglePswpClass(_thumbsContainer, 'active', !_thumbHidden);
            _thumbHidden = !_thumbHidden;
            
            if (_thumbHidden){
                $(_thumbTog).html("Close");
            } else {
                $(_thumbTog).html("Thumbnails");
            }
        },
        _thumbsTap = function(e){
            e = e || window.event;
            
            e.stopPropagation();
            
			var target = e.target || e.srcElement,
                itemNo = parseInt(target.getAttribute('data-item'));
            
            _thumbsCycle(itemNo);
            pswp.goTo(itemNo);
            _toggleThumbs();
            
        },
        _thumbsCycle = function(itemNumber){
            
            for (var g = 0; g < pswp.items.length; g++) {
            
                framework.removeClass(_thumbArray[g], 'active');

                if (g == itemNumber) {
                    framework.addClass(_thumbArray[itemNumber], 'active');
                }
            }
        };



	var _uiElements = [
		{ 
			name: 'caption', 
			option: 'captionEl',
			onInit: function(el) {
				_captionContainer = el;
			} 
		},
		{ 
			name: 'share-modal', 
			option: 'shareEl',
			onInit: function(el) {  
				_shareModal = el;
			},
			onTap: function() {
				_toggleShareModal();
			} 
		},
		{ 
			name: 'button--share', 
			option: 'shareEl',
			onInit: function(el) { 
				_shareButton = el;
			},
			onTap: function() {
				_toggleShareModal();
			} 
		},
		{ 
			name: 'button--zoom', 
			option: 'zoomEl',
			onTap: pswp.toggleDesktopZoom
		},
		{ 
			name: 'counter', 
			option: 'counterEl',
			onInit: function(el) {
				_indexIndicator = el;
			} 
		},
		{ 
			name: 'button--close', 
			option: 'closeEl',
			onTap: pswp.close
		},
		{ 
			name: 'button--arrow--left', 
			option: 'arrowEl',
			onTap: pswp.prev
		},
		{ 
			name: 'button--arrow--right', 
			option: 'arrowEl',
			onTap: pswp.next
		},
		{ 
			name: 'preloader', 
			option: 'preloaderEl',
			onInit: function(el) {  
				_loadingIndicator = el;
			} 
		},
		{ 
            name: 'gallery-title', 
            option: 'galleryTitle',
            onInit: function(el) {  
                _galleryTitle = el;
            } 
		},
		{ 
            name: 'caption-tog', 
            option: 'captionTog',
            onInit: function(el) {  
                _captionTog = el;
            },
            onTap: function(){
                framework.bind(_captionTog, 'pswpTap', _toggleCaptionContainer);
            }
		},
		{ 
            name: 'thumb-tog',
            option: 'thumbTog',
            onInit: function(el) {
                _thumbTog = el;
                framework.bind(_thumbTog, 'pswpTap', _toggleThumbs );
            },
            onTap: function(){
                //
            }
		},
		{ 
            name: 'thumbs-container',
            option: 'thumbsInner',
            onInit: function(el) {
                _thumbsContainer = el;
            }
		},
		{ 
            name: 'thumbs-inner',
            option: 'thumbsInner',
            onInit: function(el) {
                _thumbsInner = el;
            }
		}

	];

	var _setupUIElements = function() {
		var item,
			classAttr,
			uiElement;

		var loopThroughChildElements = function(sChildren) {
			if(!sChildren) {
				return;
			}
            
			var l = sChildren.length;
			for(var i = 0; i < l; i++) {
				item = sChildren[i];
				classAttr = item.className;

				for(var a = 0; a < _uiElements.length; a++) {
					uiElement = _uiElements[a];
                    
					if(classAttr.indexOf('pswp__' + uiElement.name) > -1  ) {
                        
                        if( _options[uiElement.option] ) { // if element is not disabled from options
							
							framework.removeClass(item, 'pswp__element--disabled');
							if(uiElement.onInit) {
								uiElement.onInit(item);
							}
							
							//item.style.display = 'block';
						} else {
							framework.addClass(item, 'pswp__element--disabled');
							//item.style.display = 'none';
						}
					}
				}
			}
		};
        
        loopThroughChildElements(_controls.children);
        
        
        // SET UP COUNTER
        var theCounterItem = _uiElements[4],
            theCounter = framework.getChildByClass(_captionContainer, 'pswp__caption-header');
        theCounter = framework.getChildByClass(theCounter, 'pswp__counter');
        
        if (_options[theCounterItem.option]) {
            
            framework.removeClass(theCounter, 'pswp__element--disabled');
            if(theCounterItem.onInit) {
                theCounterItem.onInit(theCounter);
            }
        } else {
            framework.addClass(theCounter, 'pswp__element--disabled');
        }
        
        
        // SET UP CAPTION TOGGLE
        var theCaptionToggleItem = _uiElements[10],
            theCaptionToggle = framework.getChildByClass(_captionContainer, 'pswp__caption-header');
        theCaptionToggle = framework.getChildByClass(theCaptionToggle, 'pswp__caption-tog');
        
        framework.removeClass(theCaptionToggle, 'pswp__element--disabled');
        theCaptionToggleItem.onInit(theCaptionToggle);
        
        
        // SET UP THUMBNAILS TOGGLE in the most inefficient way ever, find a better way of doing this
        var theThumbToggleItem = _uiElements[11],
            theThumbToggle = framework.getChildByClass(_controls, 'pswp__bottom-bar');
        theThumbToggle = framework.getChildByClass(theThumbToggle, 'pswp__bottom-bar__inner');
        theThumbToggle = framework.getChildByClass(theThumbToggle, 'tt_holder')
        theThumbToggle = framework.getChildByClass(theThumbToggle, 'pswp__thumb-tog');
        theThumbToggleItem.onInit(theThumbToggle);
        
        
        // SET UP THUMBNAILS CONTAINER
        var theThumbContainerItem = _uiElements[12],
            theThumbContainer = framework.getChildByClass(_controls, 'pswp__thumbs-container');
        
        framework.removeClass(theThumbContainer, 'pswp__element--disabled');
        theThumbContainerItem.onInit(theThumbContainer);
		
		
        // SET UP TOP BAR
        var topBar =  framework.getChildByClass(_controls, 'pswp__top-bar');
		if(topBar) {
			loopThroughChildElements( topBar.children );
		}
        
        
        // SET UP THUMBNAILS CONTROLLER
        for (var j = 0; j < pswp.items.length; j++) {
            var outerDiv = framework.createEl('thumb-control-outer quarter med-sixth', 'div');
            var newThumb = framework.createEl('thumb-control', 'img');
            newThumb.setAttribute('src', pswp.items[j].msrc);
            newThumb.setAttribute('data-item', j);
            outerDiv.appendChild(newThumb);
            _thumbsInner.appendChild(outerDiv);
            _thumbArray.push(outerDiv);
            
            framework.bind(newThumb, 'pswpTap', _thumbsTap);
        }
	};


	

	ui.init = function() {

		// extend options
		framework.extend(pswp.options, _defaultUIOptions, true);

		// create local link for fast access
		_options = pswp.options;

		// find pswp__ui element
		_controls = framework.getChildByClass(pswp.scrollWrap, 'pswp__ui');
        
        // set up thumbnails container
        _thumbsInner = framework.getChildByClass(framework.getChildByClass(_controls, 'pswp__thumbs-container'), 'pswp__thumbs-container_inner');

		// create local link
		_listen = pswp.listen;

		_setupHidingControlsDuringGestures();

		// update controls when slides change
		_listen('beforeChange', ui.update);

		// toggle zoom on double-tap
		_listen('doubleTap', function(point) {
			var initialZoomLevel = pswp.currItem.initialZoomLevel;
			if(pswp.getZoomLevel() !== initialZoomLevel) {
				pswp.zoomTo(initialZoomLevel, point, 333);
			} else {
				pswp.zoomTo(_options.getDoubleTapZoom(false, pswp.currItem), point, 333);
			}
		});

		// Allow text selection in caption
		_listen('preventDragEvent', function(e, isDown, preventObj) {
			var t = e.target || e.srcElement;
			if(
				t && 
				t.getAttribute('class') && e.type.indexOf('mouse') > -1 && 
				( t.getAttribute('class').indexOf('__caption') > 0 || (/(SMALL|STRONG|EM)/i).test(t.tagName) ) 
			) {
				preventObj.prevent = false;
			}
		});

		// bind events for UI
		_listen('bindEvents', function() {
			framework.bind(_controls, 'pswpTap click', _onControlsTap);
			framework.bind(pswp.scrollWrap, 'pswpTap', ui.onGlobalTap);

			if(!pswp.likelyTouchDevice) {
				framework.bind(pswp.scrollWrap, 'mouseover', ui.onMouseOver);
			}
		});

		// unbind events for UI
		_listen('unbindEvents', function() {
			if(!_shareModalHidden) {
				_toggleShareModal();
			}

			if(_idleInterval) {
				clearInterval(_idleInterval);
			}
			framework.unbind(document, 'mouseout', _onMouseLeaveWindow);
			framework.unbind(document, 'mousemove', _onIdleMouseMove);
			framework.unbind(_controls, 'pswpTap click', _onControlsTap);
			framework.unbind(pswp.scrollWrap, 'pswpTap', ui.onGlobalTap);
			framework.unbind(pswp.scrollWrap, 'mouseover', ui.onMouseOver);
		});


		// clean up things when gallery is destroyed
		_listen('destroy', function() {
			if(_options.captionEl) {
				if(_fakeCaptionContainer) {
					_controls.removeChild(_fakeCaptionContainer);
				}
				framework.removeClass(_captionContainer, 'pswp__caption--empty');
			}

			if(_shareModal) {
				_shareModal.children[0].onclick = null;
			}
			framework.removeClass(_controls, 'pswp__ui--over-close');
			framework.addClass( _controls, 'pswp__ui--hidden');
            
            _thumbsInner.innerHTML = '';
            
			ui.setIdle(false);
		});
		

		if(!_options.showAnimationDuration) {
			framework.removeClass( _controls, 'pswp__ui--hidden');
		}
		_listen('initialZoomIn', function() {
			if(_options.showAnimationDuration) {
				framework.removeClass( _controls, 'pswp__ui--hidden');
			}
		});
		_listen('initialZoomOut', function() {
			framework.addClass( _controls, 'pswp__ui--hidden');
		});

		_listen('parseVerticalMargin', _applyNavBarGaps);
		
		_setupUIElements();

		if(_options.shareEl && _shareButton && _shareModal) {
			_shareModalHidden = true;
		}

		_countNumItems();

		_setupIdle();

		_setupLoadingIndicator();
        
        _galleryTitle.innerHTML = _options.galleryTitle;
	};

	ui.setIdle = function(isIdle) {
		_isIdle = isIdle;
		_togglePswpClass(_controls, 'ui--idle', isIdle);
	};

	ui.update = function() {
		// Don't update UI if it's hidden
		if(_controlsVisible && pswp.currItem) {
			
			ui.updateIndexIndicator();

			if(_options.captionEl) {
				_options.addCaptionHTMLFn(pswp.currItem, _captionContainer);

				_togglePswpClass(_captionContainer, 'caption--empty', !pswp.currItem.title);
                
			}

			_overlayUIUpdated = true;

		} else {
			_overlayUIUpdated = false;
		}

		if(!_shareModalHidden) {
			_toggleShareModal();
		}

		_countNumItems();
        
        var currentIndex = pswp.getCurrentIndex();
        
        _thumbsCycle(currentIndex);
        
	};

	ui.updateIndexIndicator = function() {
		if(_options.counterEl) {
			_indexIndicator.innerHTML = (pswp.getCurrentIndex()+1) + 
										_options.indexIndicatorSep + 
										_options.getNumItemsFn();
		}
	};
	
	ui.onGlobalTap = function(e) {
		e = e || window.event;
		var target = e.target || e.srcElement;

		if(_blockControlsTap) {
			return;
		}
        
        if(e.detail && e.detail.pointerType === 'mouse') {

			// close gallery if clicked outside of the image
			if(_hasCloseClass(target)) {
				pswp.close();
				return;
			}

			if(framework.hasClass(target, 'pswp__img')) {
				if(pswp.getZoomLevel() === 1 && pswp.getZoomLevel() <= pswp.currItem.fitRatio) {
					if(_options.clickToCloseNonZoomable) {
						pswp.close();
					}
				} else {
					pswp.toggleDesktopZoom(e.detail.releasePoint);
				}
			}
			
		} else {

			// tap anywhere (except buttons) to toggle visibility of controls
			if(_options.tapToToggleControls) {
				if(_controlsVisible) {
					ui.hideControls();
				} else {
					ui.showControls();
				}
			}

			// tap to close gallery
			if(_options.tapToClose && (framework.hasClass(target, 'pswp__img') || _hasCloseClass(target)) ) {
				pswp.close();
				return;
			}
			
		}
	};
	ui.onMouseOver = function(e) {
		e = e || window.event;
		var target = e.target || e.srcElement;

		// add class when mouse is over an element that should close the gallery
		_togglePswpClass(_controls, 'ui--over-close', _hasCloseClass(target));
	};

	ui.hideControls = function() {
		framework.addClass(_controls,'pswp__ui--hidden');
		_controlsVisible = false;
	};

	ui.showControls = function() {
		_controlsVisible = true;
		if(!_overlayUIUpdated) {
			ui.update();
		}
		framework.removeClass(_controls,'pswp__ui--hidden');
	};

};
return PhotoSwipeUI_RSC;


});
